_template: page.html
---
about:

Educode 2019 est la seconde édition d’un colloque annuel international dédié à l’éducation, aux pratiques et à la recherche dans les domaines liés au numérique. Il est le résultat d’une collaboration entre différents acteurs-clé du numérique en Belgique et à l'étranger.

Le colloque Educode se déroule le vendredi 27septembre 2019 à la Haute-École Bruxelles-Brabant dans ses locaux de l'avenue Defré à Uccle (HE2B-Defré).

La conférence est **gratuite** pour les enseignants francophones membres de l'association educode. 

Pour plus d'information et la préinscription, voir http://wiki.educode.be

L'organisation de cette conférence est essentiellement *bénévole*. _Toute aide et tout soutien sont les bienvenus_. 

En particulier, vous pouvez faire des dons <script src="https://liberapay.com/npettiaux/widgets/button.js"></script>
<noscript><a href="https://liberapay.com/npettiaux/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript> 

Toute  proposition d'aide peut être envoyée à [aider@educode.be](mailto://aider@educode.be?subject=Aider educode&body=Je souhaite aider educode).
---
baseline: Une conférence internationale pour informer, former et susciter l'intérêt de tous les acteurs de l'enseignement aux défis du numérique.
---
monday: A faire : overview
---
thuesday:
---
title: Bienvenue à EduCode
---
wednesday:
---
_about_educode: About EduCode
---
_become_partner: Become partner
---
_contact: Contact
---
_expo: Exposition
---
_hotel: Hotels
---
_location: Locations
---
_monday: Monday
---
_more_infos: Tuesday
---
_our_partners: Our partners
---
_overview_program: Overview of the program
---
_partners: Partners
---
_practical_infos: Practical infos
---
_press: Press
---
_registration: Registration
---
_speakers: Speakers
---
_sunday: Sunday
---
_thuesday: Thuesday
---
_wednesday: Wednesday
---
expo_about:

Durant les pauses-café et repas, toutes les structures (associations, institutions publiques, écoles, sociétés commerciales ...) actives dans le milieu de l’éducation numérique en Belgique et ailleurs représentés lors de la conférence, présenteront leurs réalisations dans l'exposition.  

Celle-ci sera accessible essentiellement durant les pauses-cafés et repas du midi et du soir.

Ce sera l’occasion pour les enseignants, leurs directions, les élus et leurs équipes, ainsi que pour les pouvoirs publics de prendre connaissance de la grande diversité des acteurs du numérique dans le domaine de l’enseignement. Ceux-ci pourront assister et aider les écoles dans une mission d'accompagnement et de formation aux très nombreux usages du numérique.
---
partners_about:

Les partenaires de la conférence sont tous les acteurs qui considèrent que la question est importante et qui souhaitent contribuer dans la mesure de leurs moyens. Ils sont Belges et étrangers, la plupart associatifs. Les uns fournissent un indispensable soutien financier, les autres participent à la promotion et à la diffusion de l'information autour de la conférence, sont des membres du comité d'organisation de la conférence, encadrent des ateliers...

Beaucoup participent à l'exposition .
---
practical_infos:

Les participants à Educode  sont attendus dès 8h30. Les conférences et ateliers commencent à 9h00 précise.

La journée se déroulera dans les locaux de la HE2B-Defré, avenue Defré 62 à 1180 Bruxelles

Merci de vous munir des documents d’inscription pour une vérification aisée.

S'ils ne l'ont pas fait lors de leur inscription, les participants pourront commander à leur arrivée des sandwiches pour le midi. Du café, du thé et des boissons seront mis à disposition.
---
practical_infos_brussels:

Bruxelles est la capitale de la Belgique et de l'Europe. La Grand-Place médiévale est immense. Elle possède de nombreux immeubles du 17ème siècle et accueille quotidiennement des marchés aux fleurs.  Rouvert en 2006, l'Atomium, la Tour Eiffel de Bruxelles, offre une vue imprenable aussi bien à l'intérieur que sur l'extérieur. Les amateurs d'architecture ne devront pas manquer la visite du Musée Horta, aménagé dans la maison du grand architecte belge Victor Horta. Les clubs et les bars de la place St. Gery ne désemplissent jamais. Les plats de fruits de mer abondent à Sainte Catherine. Si vous souhaitez plus d'informations sur Bruxelles, cliquez [ici](http://visit.brussels) 
---
text_button: Devenir exposant
---
s_about_educode: À propos d'EduCode
---
s_become_partner: Devenez partenaire
---
s_contact: Contact
---
s_expo: Exposition
---
s_hotel: Hôtels
---
s_location: Lieux
---
s_monday: Lundi
---
s_more_infos: Plus d'infos
---
s_our_partners: Nos partenaires
---
s_overview_program: Aperçu du programme
---
s_partners: Partenaires
---
s_practical_infos: Infos pratiques
---
s_press: Presse
---
s_registration: Inscription
---
s_speakers: Intervenants
---
s_sunday: Dimanche
---
s_thuesday: Mardi
---
s_wednesday: Mercredi
---
s_intro_dates: 27 septembre 2019
---
s_intro_location_city: Bruxelles, Belgique
---
s_intro_location_institutions: HE2B-Defré
---
s_intro_location_institutions_2:
---
sunday:
---
registration:

Les préinscriptions sont possibles dans https://wiki.educode.be/doku.php?id=educode2019preinscriptions

Il a été demandée que cette formation fasse partie du programme des formations en interréseaux organisées par l’IFC. Il nous a été confirmé que selon toute vraisemblance, ce sera le cas. La conférence sera donc **gratuite pour les enseignants** de primaire et secondaire de la Fédération Wallonie-Bruxelles inscrits sur le site de IFC.  Nous attendons la confirmation de IFC pour le 20 août 2019.

---
press_text:

Une conférence de presse sera organisée début septembre 2019 à Bruxelles. Un point presse sera également organisé durant la conférence vers 13h. 

Vous pouvez également télécharger des documents utiles en cliquant sur le lien ci-dessous.
---
s_submission: Appel à soumission
---
s_submit_here: Soumettez ici
---
submissions_text:

Faites-nous part de vos propositions de stand au sein de l'exposition  en cliquant sur le bouton ci-dessous qui vous intéresse.

Les propositions d'ateliers devraient être entrées pour le 10 aout 2019 dans http://wiki.educode.be

La liste des ateliers sera publiée pour le 5 septembre 2019

---
s_submissions: Appel à propositions
---
s_may: mai
---
s_monday_27: Lundi 27
---
s_sunday_26: Dimanche 26
---
s_thuesday_28: Mardi 28
---
s_wednesday_29: Mercredi 29
---
s_thursday: Jeudi
---
s_friday: Vendredi
---
s_saturday: Samedi
---
s_program: Programme
---
practical_infos_hostels:
---
inscription_tarif: Il faut envoyer un courriel à [factures@educode.be](mailto:tfactures@educode.be?subject=Facture pour educode) pour obtenir une facture.
---
s_free_expose: Exposer et tenir un stand
---
s_propose_atelier: Proposer un atelier
---
s_submit_paper: Soumettre un poster
---
contact:

Les plus technophiles d'entre vous (soit vous tous ;-), qui voulez apprendre et comme les apprenants de BeCentral qui le font, si mes informations sont correctes, dès les tous premiers jours de leur formation) peuvent le faire en (vous apprécierez l'algorithme, d'apprendre un peu le code et ce qui accompagne, et de pouvoir taper un peu dans un terminal ;-)

*    installant sur votre machine (windows ou macos x ou gnulinux)
 *        le logiciel libre avec lequel le site est fait Lektor (voir https://getlektor.org; c'est beaucoup plus facile sur une machine gnulinux que macox x ou windows, sans être difficile sur celles-ci)
 *      le logiciel libre git (voir https://git-scm.com/download/)
*    en vous créant un login sur http://github.com
*    en allant sur https://github.com/EduCodeBe/educode.be et en forkant le projet (en haut à droite)
*    tapant dans le terminal (git-bash sur windows)
   *     git clone https://github.com/VOTRE_LOGIN/educode.be
   *     cd <le répertoire où vous êtes>/educode.be/EduCode
   *     lektor server
   *     firefox http://localhost:5000/
   *     (vous pouvez alors
      *       éditer le site avec une jolie interface graphique dans le navigateur et un petit crayon à droite qui permet d'éditer, et
      *       aller par exemple sur la page http://localhost:5000/admin/root:exposants/edit
      *       ajouter une page "exposant" en cliquant sur la première ligne sur + et en ajoutant une page dont le modèle est "Exposant"
      *       en complétant les champs au format markdown  https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet assez facile
      *       en sauvant localement sur votre machine au bas de la page
      *       si vous voulez aider à une traduction de n'importe quelle page, c'est ici que cela se passe en allant dans le menu à gauche de la langue. Les textes ont été traduits en espagnol déjà mais je n'ai pas eu le temps de les incorporer. Ils sont dans )
      *      arrêtant le serveur local par CONTROL-C
*     en faisant tout remonter sur le serveur central en tapant
  *           git add *
  *          git commit -m "une description de vos modifications et ajouts"
  *          git push (vous rentrez alors vos logins et mots de passe)
  *        en me prévenant que vous voulez que nous intégrions vos modifications par mail à site@educode.be
  *     et si possible en faisant sur github un pull request (qui simplifie les procédures 6 di-dessus et suivantes pour moi)
    *         vous allez sur votre page https://github.com/VOTRE_LOGIN/educode.be/pulls
    *            vous cliquez à droite sur le bouton NEW PULL REQUEST et vérifiez que votre projet est bien lié à EducodeBe/educode.be (le master)
    *            vous complétez un petit mot
    *            et envoyer
    *    j'accepte votre proposition si il n'y a pas de conflit et hop
    *     je mets en ligne dès que je peux (ou un autre membre de l'équipe le fait)
        comme cela, vraiment, tous ensemble nous mettons en œuvre la collaboration pour le site et tout le reste.

#     La mise en pratique de la théorie et le numérique, c'est aussi pour nous !
---
concert:

Un soirée exceptionnelle dans un lieu d'exception
- 19h Conférence de [Gilles Dowek](https://wiki.educode.be/doku.php/personnes/gilles_dowek) sur la transformation au numérique des écoles françaises
- 20h Le _carnaval des animaux_ de Camille Saint-Saëns, concert festif avec 2 pianos et récitante
- 21h Cocktail dînatoire

Voir l'[affiche](https://wiki.educode.be/lib/exe/fetch.php/educode_2019/concertcarnavaldesanimaux.png)

[Plus d'informations...](concert)
---
concert_avant_premiere:

Une avant première du [concert festif](/concert) en petit commité.

[Plus d'informations...](concert-festif-avant-premiere)
---
journee_principale:

Educode 2019 est la seconde édition d’un colloque annuel international dédié à l’éducation, aux pratiques et à la recherche dans les domaines liés au numérique. Son ambition est d'__informer, former et susciter l'intérêt de tous les acteurs de l'enseignement aux défis du numérique__.

La conférence est gratuite pour les enseignants francophones et inscrit à IFC avec le **numéro de formation 03001901/34350** à introduire dans le site [www.ifc.cfwb.be](http://www.ifc.cfwb.be/)

L'organisation de cette conférence est essentiellement bénévole. Toute don est le bienvenu.

Localisation sur une [carte](https://osm.org/go/0EoSK76Bo--?m=)

[Plus d'informations...](journee-principale)
---
journee_sans_ordi:

Une journée spécialement dédiée aux enseignants intéressés par l'informatique débranchée.

La journée est gratuite pour les enseignants francophones membres de l'association educode et inscrit à IFC avec le numéro de formation 03001901/34350 à introduire dans le site http://www.ifc.cfwb.be/. Pour toutes les autres personnes, elle est payante.

Pour plus d'information et la préinscription, voir http://wiki.educode.be ou par email à info@educode.be

L'organisation de cette conférence est essentiellement bénévole. Toute aide et tout soutien sont les bienvenus.

[Plus d'informations...](journee-info-sans-ordi)
