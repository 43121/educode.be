_model: details-program
---
date: 27 september 2019
---
description:

Leraren en hun directies kunnen conferenties, een rondetafel bijwonen en deelnemen aan vele praktische workshops waarin zij de leerlingen zijn, om hun alter ego's te vinden, inhoud en ervaringen met hen te hergebruiken in de klas wat ze hebben geleerd. Deze hands-on workshops zullen zich richten op een breed scala aan onderwerpen rond onderwijs en het gebruik van digitaal en computerdenken, en nieuwe manieren van werken die digitaal zijn.

Tijdens de pauzes kunnen ze een groot aantal actoren ontmoeten, voornamelijk verenigingen, die activiteiten aanbieden die verband houden met digitaal en training. Oplossingen voor kinderopvang met activiteiten worden georganiseerd zodat alle ouders kunnen deelnemen.

#Introductie
De conferentie is open en algemeen, en is niet beperkt tot codering, zelfs als de aspecten met betrekking tot het oplossen van problemen, inzicht in de mechanismen van de werking van computers, juridische en ethische kwesties in verband met het gebruik van digitale enz. zijn erg belangrijk.

#Objectifs
Het doel is om een nieuwe dag te organiseren rond digitaal onderwijs en onderwijs via digitaal op alle niveaus (primair, secundair en hoger), met een gedeelte gewijd aan programmeren.

Voor zover mogelijk worden lerarendagen georganiseerd met de steun van IFC om door hen te worden erkend als verplichte dagen voor voortgezet onderwijs voor leraren.

Leraren, directeurs, ouders en studenten, onderzoekers en iedereen die geïnteresseerd is in lesgeven en digitaal, evenals in permanente educatie worden uitgenodigd om na te denken over het gebruik van digitale technologie bij het lesgeven en leren via digitale technologie van alle niveaus (primair, secundair, hoger en sociale vooruitgang), met een aanzienlijk deel gewijd aan programmeren. Er zullen ook veel praktische trainingen worden georganiseerd.

Het evenement bestaat uit 3 delen voor 4 doelgroepen:

1. Conferenties, workshops en tentoonstellingen voornamelijk voor leerkrachten
2. Een opening voor het grote publiek met tentoonstelling en conferenties
3. Een conferentie en activiteiten gewijd aan schoolhoofden
4. Een conferentie gewijd aan wetenschappelijk onderzoek op het gebied van pedagogiek, onderwijs en digitaal

#Content

Sommige conferenties, maar meestal praktische workshops en presentaties, vooral door leraren en hun studenten, of door vertegenwoordigers van verschillende gespecialiseerde bedrijven of verenigingen die bijdragen aan de training in het gebruik van ICT op school (zoals HE2B, ULBrussels, ULiège, UNamur, UMons, USaintLouis, Devoxx4kids, SICarré, het computerbedrijf van Frankrijk, Class'Code, the Scientothèque, Fablabe'ke, OpenFab, WeAreCoders, Reservewiel, BxLug, Abelli, CESEP, ForSud ...) . De stuurgroep van de conferentie wil de actoren van het onderwijs en het Pact of excellentie zijn kennis van de actoren en zijn capaciteit voor het organiseren van vergaderingen tussen professionals van verschillende horizonten ten dienste stellen.
<div class="col-12 text-center">
                        <a href="../ateliers" class="btn btn-lg btn-warning text-uppercase">Voir les ateliers</a>
</div>

#Localisation
Op de middelbare school Brussel-Brabant, Categorie Defré, De Frélaan in 1180 Ukkel, zeer gemakkelijk bereikbaar met het openbaar vervoer, auto en fiets.

#Activiteiten
- 9u - 17u: kleine lezingen en presentaties voor docenten en geïnteresseerden. Tijdens pauzes, tonen scholen, verenigingen, bedrijven hun prestaties en projecten met betrekking tot digitaal en onderwijs, en projecten gemaakt in klaslokalen (in gangen en andere ruimtes)
- 14: 00 tot 15: 00: Round Table Education and Technology in the World: Wat werkt en wat niet, en waarom, wat zou kunnen worden gedaan (met nationale vertegenwoordigers uit verschillende landen)
- 15.00 uur tot 15.30 uur: Conferentie door Françoise Bols (HE2B en ULiège): Onderwijs in de digitale en informatica op de middelbare school: uitdagingen en kansen
2. Een opening voor het grote publiek met tentoonstellingen en conferenties - 15.30 uur tot 16.00 uur: conferentie **Staat van onderwijs en digitaal onderzoek** door Julie Henry (Unamur)
- 16: 00 uur tot 18: 00 uur: vertoning van de film **The battle of the free** gevolgd door een discussie met de regisseur, Philippe Borel
- 9u tot 17u: er worden meer dan 30 praktische workshops georganiseerd (er worden 100 computers geïnstalleerd in vergaderzalen en deelnemers worden uitgenodigd om met hun laptops te komen)
- 17.00 tot 18.00 uur: buffet sluiten

#Organisatiecomité
- Dr Nicolas Pettiaux (ESI-HE2B & ULB-Lisa, assistent-master)
- Françoise Bols (HE2B-Nivelles, lector)
- Erick Mascart (Forsub, educatieve adviseur en trainer)
- Eric Robette (HE2B-Defré, assistent-master)
- Denis Longrée (president van Roue de secours asbl, meester in wiskunde)

#Programmacommissie 
- Françoise Bols
- Laurence Bourguignon (Forsud, onderwijsadviseur - TICE trainer en leerkracht basisschool, op Koninklijk Atheneum van Couvin - fundamenteel)
- Prof Olivier Debeir (ULB)
- Dr Pierre De Buyl (KU Leuven, onderzoeker)
- Julie Henry (UNamur, onderzoeker)
- Georges Khaznadar (Lycee Jean Bart, Duinkerken)
- Denis Longrée
- Erick Mascart
- Prof Thierry Massart (ULB)
- Dr Nicolas Pettiaux
- Eric Robette

# Wetenschappelijk en ondersteunend comité
- Prof Hugues Bersini (ULB)
- Prof Bibiana Boccolini (Universiteit van Rosario, Argentinië)
- Prof Colin de la Higuera (Universiteit van Nantes)
- Éric Deprins, voormalig CEO van Toyota Belgium, Unilever, Mestdagh
- Olivier Goletti (Ulouvain & SIcarre, onderzoeker)
- Denis Matagne (HE2B-Defré, directeur) 
- Alexia Pasini (HE2B, co-voorzitter)
- Yves Robaey (HE2B, co-voorzitter)
- Prof. Bernard Rentier (ULiège, voormalig rector)
- Karin Van Loon (HE2B-ISIB, director)
- Michel Willemse (HE2B-ESI, directeur)
- Prof Pierre Wolper (Uliège, rector)

#Contact
Nicolas Pettiaux, voorzitter van de Educode vzw, nicolas@educode.be, mobiel 496 24 55 01
