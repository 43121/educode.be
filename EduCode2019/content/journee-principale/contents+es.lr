_model: details-program
---
date: 27 septembre 2019
---
description:

Les enseignants et leurs directions pourront suivre des conférences, une table-ronde et participer à de nombreux ateliers pratiques durant lesquels ils seront les apprenants, de manière à retrouver leurs alter-egos, développer avec eux des contenus et des expériences pour pouvoir réutiliser en classe ce qu'ils ont appris. Ces ateliers d’échanges pratiques concerneront des sujets très variés autour de l’éducation et de l’usage du numérique et de la pensée informatique, et de nouvelles manières de travail adaptées au numérique.

Durant les pauses, ils pourront rencontrer de très nombreux acteurs, essentiellement associatifs, qui proposent des activités liées au numérique et à la formation. Des solutions de garde d'enfants avec des activités seront organisées pour que tous les parents puissent participer.

﻿#Introduction
La conférence est ouverte et généraliste, et ne se limite pas au codage, même si les aspects liés à la résolution de problèmes, à la compréhension des mécanismes de fonctionnement des ordinateurs, des questions légales et éthiques liées à l'usage du numérique etc. sont très importantes. Elles devraient en 2019 faire partie des enseignements généraux du citoyen du XXI siècle.

#Objectifs
L’objectif est d’organiser à nouveau une journée, autour de l’enseignement du numérique et de l’enseignement grâce au numérique tous niveaux confondus (primaires, secondaire et supérieur), avec une partie consacrée à la programmation .

Les journées consacrées aux enseignants seront, autant que possible, organisées avec le soutien de IFC pour être  reconnues par elles comme journées de formation continue obligatoires pour les enseignants.

Les professeurs, directeurs d'école, parents et étudiants, chercheurs et toute personne intéressée par l'enseignement et le numérique comme par la formation continue sont invités à réfléchir sur l'usage du numérique dans l'enseignement et l’enseignement grâce au numérique tous niveaux confondus (primaire, secondaire, supérieur et enseignement de promotion sociale), avec une partie importante consacrée à la programmation. De nombreuses formations pratiques seront également organisées.

L’évènement comporte 3 parties pour 4 publics :

1. Des conférences, ateliers et expositions destinés essentiellement aux enseignants
2. Une ouverte au grand public avec exposition et conférences
3. Une conférence et des activités dédiées aux directeurs d’établissement
4. Une conférence consacrée à la recherche scientifique sur la pédagogie, l'enseignement et le numérique

#Contenu

Quelques conférences mais surtout des ateliers pratiques et des présentations, notamment par des enseignants  et leurs étudiants, ou par des représentants des diverses sociétés spécialisées ou des associations qui contribuent à la formation à l'utilisation des TICE à l'école (telles que HE2B, ULBruxelles, ULiège, Unamur, UMons, USaintLouis, Devoxx4kids, SICarré, La société informatique de France, Class'Code, la  Scientothèque, Fablabe'ke, OpenFab, WeAreCoders, Roue de secours, BxLug, Abelli, CESEP, ForSud ... ). L'équipe de pilotage de la conférence souhaite mettre au service des acteurs de l'enseignement et du Pacte d'excellence sa connaissance des acteurs et sa capacité d'organisation de rencontres entre professionnels d'horizon divers.
<div class="col-12 text-center">
                        <a href="../ateliers" class="btn btn-lg btn-warning text-uppercase">Voir les ateliers</a>
</div>

#Localisation
À la haute école Bruxelles-Brabant, Catégorie Defré, avenue Defré à 1180 Uccle, très aisément accessible en transports en commun, voiture et vélo.

#Activités
- 9h - 17h : petites conférences et exposés pour les enseignants et toute personne intéressée. Durant les pauses, les écoles, associations, entreprises exposent leurs réalisations et projets liés au numérique et à l'éducation, et des projets faits dans les classes (dans les couloirs et autres espaces)
- 14h00 à 15h00 : Table-ronde Enseignement et technologie dans le monde : ce qui marche ou pas, et pourquoi, ce qui pourrait être fait (avec des représentants nationaux de différents pays)
- 15h00 à 15h30 : Conférence par Françoise Bols (HE2B et ULiège) : Enseigner le numérique et l'informatique en haute école : défis et opportunités
- 15h30 à 16h00 : Conférence **État de la recherche en éducation et numérique** par Julie Henry (Unamur)
- 16h00 à 18h00 : Projection du film **La bataille du libre** suivie d'une discussion avec son réalisateur, Philippe Borel
- 9h à 17h : plus de 30 ateliers pratiques sont organisés (100 ordinateurs seront installés dans des locaux de séminaires, et les participants seront invités à venir avec leurs portables)
- 17h à 18h : buffet de clôture

#Comité d’organisation
- Dr Nicolas Pettiaux (ESI-HE2B & ULB-Lisa, maître-assistant)
- Françoise Bols (HE2B-Nivelles, chargé de cours)
- Erick Mascart (Forsub, conseiller pédagogique et formateur)
- Eric Robette (HE2B-Defré, maître-assistant)
- Denis Longrée (président de roue de secours asbl, maitre en mathématique)

#Comité de programme 
- Françoise Bols
- Laurence Bourguignon (Forsud, conseillère pédagogique - formatrice TICE et institutrice primaire, à Athénée Royal de Couvin - fondamental)
- Prof Olivier Debeir (ULB)
- Dr Pierre De Buyl (KU Leuven, chercheur)
- Julie Henry (UNamur, chercheuse)
- Georges Khaznadar (Lycée Jean Bart, Dunkerque)
- Denis Longrée
- Erick Mascart
- Prof Thierry Massart (ULB)
- Dr Nicolas Pettiaux
- Eric Robette

# Comité scientifique et de soutien
- Prof Hugues Bersini (ULB)
- Prof Bibiana Boccolini (Université de Rosario, Argentine)
- Prof Colin de la Higuera (Université de Nantes)
- Éric Deprins, ex CEO de Toyota Belgium, Unilever, Mestdagh
- Olivier Goletti (Ulouvain & SIcarre, chercheur)
- Denis Matagne (HE2B-Defré, directeur) 
- Alexia Pasini (HE2B, co-présidente)
- Yves Robaey (HE2B, co-président)
- Prof Bernard Rentier (Uliège, ancien recteur)
- Karin Van Loon (HE2B-ISIB, directrice)
- Michel Willemse (HE2B-ESI, directeur)
- Prof Pierre Wolper (Uliège, recteur)

#Contact
Nicolas Pettiaux, président de l’asbl educode, nicolas@educode.be, gsm 496 24 55 01
